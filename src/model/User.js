// module.exports = (mongoose) => {

// }
module.exports = mongoose => {
  const shortid = require("shortid");
  const Schema = mongoose.Schema;

  const pointSchema = new Schema({
    type: {
      type: String,
      enum: ["Point"],
      required: true
    },
    coordinates: {
      type: [Number],
      required: true
    }
  });

  const userSchema = new Schema({
    username: { type: String, default: shortid.generate() },
    name: String,
    coins: {
      type: Number,
      default: 1000
    },
    userId: {
      type: Number,
      required: true
    },
    signedup: {
      type: Boolean,
      default: false
    },
    refers: {
      referer: String,
      refers: [String]
    },
    acceptedTerms: {
      type: Boolean,
      default: false
    },
    sex: {
      type: String,
      default: "female"
    },
    state: String,
    height: { type: Number, default: 100 },
    age: { type: Number, default: 99 },
    location: pointSchema
  });

  userSchema.index({ location: "2dsphere" });

  const User = mongoose.model("User", userSchema);
  return User;
};
