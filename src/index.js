const telegraf = require("telegraf");
const config = require("./config");
const LocalSession = require('telegraf-session-local')

const { User } = require("./model");
const { stage, scene_keys } = require("./scenes");
const { branch } = telegraf;

const session = (new LocalSession({ database: 'session.json' })).middleware()

var bot = new telegraf(config.bot_token);
bot.use(session);
bot.use(async (ctx, next) => {
  var user = await User.findOne({
    userId: ctx.from.id
  });
  if (user == undefined) {
    user = new User({
      userId: ctx.from.id
    });
    user = await user.save();
  }
  ctx.user = user;
  next();
});

bot.use(stage.middleware());


//referal link implementation here
// bot.hears(/\/start .*/,(ctx,next) => {
//     console.log(ctx.match)
// })

bot.hears(
  /\/start/,
  branch(
    ctx => {
      console.log(ctx.user.username);
      return ctx.user.signedup;
    },
    ctx => {
      console.log("ctx.user.signedup : true");
      ctx.reply("you are already signed up");
    },
    ctx => ctx.scene.enter(scene_keys.signup)
  )
);

bot.launch();
