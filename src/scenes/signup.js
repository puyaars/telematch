const Scene = require("telegraf/scenes/base");
const Composer = require("telegraf/composer");
const Markup = require("telegraf/markup");
const config = require("../config");
const WizardScene = require("telegraf/scenes/wizard");

const sexHandler = new Composer();

sexHandler.use((ctx, next) => {
  console.log(ctx.message.text);
  next();
});

sexHandler.hears(config.questions.sex_question.a[0], async ctx => {
  ctx.scene.state.gender = "male";
  ctx.user.sex = "male";
  await ctx.user.save();
  if (ctx.scene.state.curser != undefined) return next();
  ctx.scene.state.curser = 0;
  askNext(ctx);
  ctx.wizard.next();
});

sexHandler.hears(config.questions.sex_question.a[1], async ctx => {
  ctx.scene.state.gender = "female";
  ctx.user.sex = "female";
  await ctx.user.save();
  if (ctx.scene.state.curser != undefined) return next();
  ctx.scene.state.curser = 0;
  askNext(ctx);
  ctx.wizard.next();
});

sexHandler.use(ctx => ctx.reply("متوجه نشدم..."));

const questionHandler = new Composer();

const askNext = (ctx, item) => {
  if (item == undefined)
    item = config.questions[ctx.scene.state.gender][ctx.scene.state.curser];
  var keys = [];
  if (item.a == "state") {
    config.states.forEach(element => {
      if (keys.length == 0 || keys[keys.length - 1].length == 3) {
        keys.push([]);
      }
      keys[keys.length - 1].push(element);
    });
  } else if (/^nums-/.test(item.a)) {
    var [_, min, max, step] = item.a.split("-");
    min = +min;
    max = +max;
    step = +step;
    while (min <= max) {
      if (keys.length == 0 || keys[keys.length - 1].length == 3) {
        keys.push([]);
      }
      keys[keys.length - 1].push(`${min}`);
      min = min + step;
    }
  } else if (item.a == "location") {
    keys.push(
      'بیخیال نمیخام بدونی',
      Markup.locationRequestButton("ارسال موقعیت")
    );
  } else {
    keys = item.a;
  }
  var keyboard = Markup.keyboard(keys)
    .resize()
    .extra();
  ctx.reply(item.q, keyboard);
};

questionHandler.on("text", ctx => {
  if (ctx.scene.state.qas == undefined) ctx.scene.state.qas = [];
  ctx.scene.state.qas.push({
    q: config.questions[ctx.scene.state.gender][ctx.scene.state.curser].q,
    a: ctx.message.text
  });
  console.log(ctx.scene.state.qas);
  ctx.scene.state.curser++;
  if (
    ctx.scene.state.curser == config.questions[ctx.scene.state.gender].length
  ) {
    askNext(ctx, config.questions.state_question);
    return ctx.wizard.next();
  }

  askNext(ctx);
});

const stateHandler = new Composer();

stateHandler.hears(config.states, async ctx => {
  console.log(ctx.message.text);
  console.log(ctx.match);

  ctx.user.state = ctx.match;
  await ctx.user.save();
  askNext(ctx, config.questions.location_question);
  return ctx.wizard.next();
});

stateHandler.use(ctx => ctx.reply("متوجه نشدم..."));

const locationHandler = new Composer();

locationHandler.on('location',async ctx => {
    ctx.user.location = {type: 'Point',"coordinates":[ctx.message.location.latitude,ctx.message.location.longitude]}
    await ctx.user.save()
    askNext(ctx,config.questions.height_question)
    return ctx.wizard.next();
})

locationHandler.hears('بیخیال نمیخام بدونی',(ctx)=> {
    askNext(ctx,config.questions.height_question)
    return ctx.wizard.next();
})

locationHandler.use(ctx => ctx.reply("متوجه نشدم..."));


// const ageHandler = new Composer();

// ageHandler.hears(/^\d+&/,async ctx => {
//     ctx.user.height = +ctx.match[0]
//     await ctx.user.save()
//     askNext(ctx,config.questions.age_question)
//     return ctx.wizard.next();
// })

const heightHandler = new Composer();

heightHandler.hears(/^\d+$/,async ctx => {
    ctx.user.height = +ctx.match[0]
    await ctx.user.save()
    askNext(ctx,config.questions.age_question)
    return ctx.wizard.next();
})

heightHandler.use(ctx => ctx.reply("متوجه نشدم..."));

const ageHandler = new Composer();

ageHandler.hears(/^\d+$/,async ctx => {
    ctx.user.age = +ctx.match[0]
    ctx.user.signedup = true
    await ctx.user.save()
    ctx.reply('puyaars.ir',Markup.keyboard([['جستو جوی تصادفی', 'هم استانی ها'],['افراد نزدیک','پروفایل']]).resize().extra())
    return ctx.wizard.next();
})

ageHandler.use(ctx => ctx.reply("متوجه نشدم..."));

module.exports = id => {
  const scene = new WizardScene(
    id,
    ctx => {
      ctx.reply(
        config.questions.sex_question.q,
        Markup.keyboard([config.questions.sex_question.a])
          .resize()
          .extra()
      );
      ctx.wizard.next();
    },
    // (ctx) => {console.log(ctx.message.text)}
    sexHandler,
    questionHandler,
    stateHandler,
    locationHandler,
    heightHandler,
    ageHandler
  );

  return scene;
};
