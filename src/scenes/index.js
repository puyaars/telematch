const Stage = require('telegraf/stage')
const { leave } = Stage
const stage = new Stage()
stage.command('cancel', leave())

const scene_keys = {
    signup : "signup_scene"
}

stage.register(require('./signup')(scene_keys.signup))

module.exports = {stage,scene_keys}